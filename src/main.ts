const {
  BOT_TOKEN, // discord app bot token
  GUILD_ID, // ID of guild to watch
  CATEGORY_ID, // category channel ID of category to watch
} = require("../secrets.json");

const NUM_UTC_OFFSETS: number = 38;

const spacetime = require("spacetime");

const Discord = require("discord.js");
const bot = new Discord.Client();

const isNightInZone = (zone: string): boolean => spacetime.now(zone).hour() < 6;

(async () => {
  await new Promise((resolve, reject) => {
    bot.on("ready", function () {
      resolve(undefined);
    });
    bot.login(BOT_TOKEN);
  });

  const guild =
    bot.guilds.cache[GUILD_ID] || (await bot.guilds.fetch(GUILD_ID));
  console.log(`Using guild ${GUILD_ID}: ${guild.name}`);

  const category =
    guild.channels.cache[CATEGORY_ID] ||
    (await guild.channels.resolve(CATEGORY_ID));
  console.log(`Using category ${CATEGORY_ID}: ${category.name}`);

  const roles = await guild.roles.fetch();

  setInterval(() => {
    roles.cache.forEach((role) => {
      if (!role.name.includes("UTC")) {
        return;
      }
  
      const zone = role.name;
      const id = role.id;
  
      if (isNightInZone(zone)) {
        console.log("Zone is in night: " + zone);
        category.createOverwrite(role.id, {
          VIEW_CHANNEL: true,
        });
      } else {
        category.createOverwrite(role.id, {
          VIEW_CHANNEL: false,
        });
      }
    });
  }, 2 * 60 * 1000);
  
  // TODO sanity check to ensure no user has two UTC roles
  // TODO discovery function that prints the IDs of the guild, its channels, and its roles when starting up
})();
